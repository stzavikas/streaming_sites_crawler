from gamato_parser.MainPageParser import MainPageParser
from gamato_parser.GamatoParser import GamatoParser
from gamato_parser.HdvidParser import HdvidParser
from gamato_parser.VidziParser import VidziParser
from gamato_parser.StreamintoParser import StreamintoParser


if __name__ == "__main__":
    sp = StreamintoParser()
    print(sp.parse_url('http://streamin.to/ys71s51oscv1'))
    # vp = VidziParser()
    # print(vp.parse_url('http://vidzi.tv/uhaq6vkehb8p.html'))
    # hdp = HdvidParser()
    # print(hdp.parse_url('http://hdvid.tv/034wop2364e0.html'))
    # xp = MainPageParser()
    # print(xp.parse_url('http://xrysoi.se/westworld-2016/', download_files=False))
    # gp = GamatoParser()
    # print(gp.parse_url('http://gamatotv.me/group/little-miss-sunshine-2006', download_files=False))