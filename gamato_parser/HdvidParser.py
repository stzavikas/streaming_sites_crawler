import re
import urllib.request
import urllib.parse
from time import sleep


class HdvidParser(object):
    def __init__(self):
        pass

    def parse_url(self, web_url):
        req = urllib.request.Request(web_url,
                                     data=None,
                                     headers={'User-Agent': 'Mozilla/5.0'})
        response = urllib.request.urlopen(req)
        content = response.read().decode('utf-8')

        if not re.search('id=\"btn_download\"', content):
            raise Exception('File Not Found or removed')

        form_values = {}
        for i in re.finditer(
                '<input.*?name="(.*?)".*?value="(.*?)">', content):
            form_values[i.group(1)] = i.group(2)

        form_data = urllib.parse.urlencode(form_values).encode('utf-8')
        sleep(3)

        req = urllib.request.Request(web_url,
                                     data=form_data,
                                     headers={'User-Agent': 'Mozilla/5.0'})
        content = urllib.request.urlopen(req).read().decode('utf-8')

        r = re.search('\[\{file:"(.+?)",', content)

        if r:
            return form_values["fname"], r.group(1)
        else:
            raise Exception('File Not Found or removed')
