from flask import Flask, render_template, request, abort, send_from_directory, Response
from gamato_parser.MainPageParser import MainPageParser
from gamato_parser.GamatoParser import GamatoParser
import requests
import os


app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def main_page_method():
    return render_template('mainPage.html')


@app.route('/result',methods=['POST', 'GET'])
def result():
    if request.method == 'POST':
        request_url = request.form["URL"]
        try:
            if "xrysoi" in request_url or 'gamatotv' in request_url:
                link_result = MainPageParser().parse_url(request_url)
                return render_template("result.html", result=link_result)
            else:
                link_result = "Unsupported website."
        except Exception as e:
            print("Exception during parsing: " + str(e))
            return abort(400)
    else:
        return abort(400)


@app.route('/download', methods=["POST"])
def download_url():
    if request.method == 'POST':
        url = request.form["URL"][:-1]
        filename = request.form["filename"][:-1]
        binary_stream = requests.get(url, stream=True)
        headers = { 'Content-Disposition': 'attachment; filename=' + filename }
        for header in ('content-type', 'content-length', 'transfer-encoding'):
            if header in binary_stream.headers:
                headers[header] = binary_stream.headers[header]
        return Response(binary_stream,
                        headers=headers)
    else:
        return abort(400)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

