import re
import urllib.request
import urllib.parse
from time import sleep
import jsbeautifier


class StreamintoParser(object):
    def __init__(self):
        pass

    def parse_url(self, web_url):
        req = urllib.request.Request(web_url,
                                     data=None,
                                     headers={'User-Agent': 'Mozilla/5.0'})

        response = urllib.request.urlopen(req)
        content = response.read().decode('utf-8')

        if not re.search('id=\"btn_download\"', content):
            raise Exception('File Not Found or removed')

        form_values = {}
        for i in re.finditer(
                '<input.*?name="(.*?)".*?value="(.*?)">', content):
            form_values[i.group(1)] = i.group(2)

        form_data = urllib.parse.urlencode(form_values).encode('utf-8')
        # Wait for 5 seconds + some more as a failsafe
        sleep(5)

        req = urllib.request.Request(web_url,
                                     data=form_data,
                                     headers={'User-Agent': 'Mozilla/5.0'})

        content = urllib.request.urlopen(req).read().decode('utf-8')
        start_marker = content.find("<script type=\'text/javascript\'>eval")
        if start_marker == -1:
            raise Exception("Invalid StreamingTo video")
        video_js = content[start_marker:]
        end_marker = video_js.find("</script>")
        video_js = video_js[:end_marker]
        video_js = video_js.replace("<script type=\'text/javascript\'>", "")
        beautiful_video = jsbeautifier.beautify(video_js)
        r = re.search('file: "(.+?)"', beautiful_video)

        if r:
            # TODO: Fix naming of the file
            return 'tempfile.flv', r.group(1)
        else:
            raise Exception('File Not Found or removed')
