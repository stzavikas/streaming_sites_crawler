import urllib.request
import re
import multiprocessing.dummy
from gamato_parser.StreamcloudParser import StreamcloudParser


class GamatoParser(object):
    def __init__(self):
        pass

    def parse_url(self, web_url, download_files=False):
        req = urllib.request.Request(web_url,
                                     data=None,
                                     headers={'User-Agent': "Magic Browser"})
        response = urllib.request.urlopen(req)
        content = response.read().decode('utf-8')

        streamcloud_urls = []
        for i in re.finditer(
                '<a href=\"http://streamcloud.eu/(.*?)\" target=\"_blank\"', content):
            streamcloud_urls.append("http://streamcloud.eu/" + str(i.group(1)))

        if len(streamcloud_urls) <= 0:
            raise Exception("No streamcloud links in the page.")

        result_dict = {}
        download_errors = []
        p = multiprocessing.dummy.Pool(20)

        def download_video_url(url):
            sp = StreamcloudParser()
            try:
                video_url = sp.parse_url(url)
                video_file = url.split("/")[-1].replace(".html", "")
                result_dict[video_file] = video_url
                if download_files:
                    urllib.request.urlretrieve(video_url, video_file)
            except Exception as e:
                download_errors.append("URL " + str(url) + " failed to download. Exception message: " + str(e))

        p.map(download_video_url, streamcloud_urls)
        return result_dict, download_errors
