import urllib.request
import re
import multiprocessing.dummy
from multiprocessing import Pool
from collections import OrderedDict, namedtuple
from time import sleep

from gamato_parser.HdvidParser import HdvidParser
from gamato_parser.StreamcloudParser import StreamcloudParser
from gamato_parser.VidziParser import VidziParser
from gamato_parser.StreamintoParser import StreamintoParser

StreamLinkContainer = namedtuple('StreamLinkContainer', ['streamcloud_links', 'streamcloud_errors',
                                                         'hdvid_links', 'hdvid_errors',
                                                         'vidzi_links', 'vidzi_errors',
                                                         'streaminto_links', 'streaminto_errors'])


class MainPageParser(object):
    def __init__(self):
        pass

    def _extract_video_links(self, content, stream_service_url, parser, thread_nr):
        try:
            video_stream_urls = []
            link_regex = 'href=\"' + stream_service_url + '(.*?)\"'
            for i in re.finditer(link_regex, content):
                video_stream_urls.append(stream_service_url + str(i.group(1)))

            download_errors = []
            if len(video_stream_urls) <= 0:
                raise Exception("No " + stream_service_url + " links in the page.")
        except Exception as e:
            print("Exception when extracting " + stream_service_url + " URLs: " + str(e))
            return OrderedDict(), []

        result_dict = OrderedDict()
        p = multiprocessing.dummy.Pool(thread_nr)
        lock = multiprocessing.Lock()

        def download_video_url(url):
            try:
                lock.acquire()
                sleep(0.1)
                lock.release()
                video_file, video_url = parser.parse_url(url)
                result_dict[video_file] = video_url
            except Exception as e:
                download_errors.append("URL " + str(url) + " failed to download. Exception message: " + str(e))

        p.map(download_video_url, video_stream_urls)
        return result_dict, download_errors

    def parse_url(self, web_url, download_files=False):
        req = urllib.request.Request(web_url,
                                     data=None,
                                     headers={'User-Agent': 'Mozilla/5.0'})
        response = urllib.request.urlopen(req)
        content = response.read().decode('utf-8')

        pool = Pool(processes=20)
        scloud_async_res = pool.apply_async(self._extract_video_links, args=(content, "http://streamcloud.eu/", StreamcloudParser(), 80))
        hdvid_async_res = pool.apply_async(self._extract_video_links, args=(content, "http://hdvid.tv/", HdvidParser(), 3))
        vidzi_async_res = pool.apply_async(self._extract_video_links, args=(content, "http://vidzi.tv/", VidziParser(), 5))
        streaminto_async_res = pool.apply_async(self._extract_video_links, args=(content, "http://streamin.to/", StreamintoParser(), 5))

        scloud_res = scloud_async_res.get(timeout=120)
        hdvid_res = hdvid_async_res.get(timeout=120)
        vidzi_res = vidzi_async_res.get(timeout=120)
        streaminto_res = streaminto_async_res.get(timeout=120)

        link_container = StreamLinkContainer(streamcloud_links = scloud_res[0],
                                             streamcloud_errors = scloud_res[1],
                                             hdvid_links = hdvid_res[0],
                                             hdvid_errors = hdvid_res[1],
                                             vidzi_links = vidzi_res[0],
                                             vidzi_errors = vidzi_res[1],
                                             streaminto_links=streaminto_res[0],
                                             streaminto_errors=streaminto_res[1])
        print(link_container)
        return link_container

