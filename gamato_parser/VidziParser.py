import re
import urllib.request
import urllib.parse
import jsbeautifier


class VidziParser(object):
    def __init__(self):
        pass

    def parse_url(self, web_url):
        try:
            req = urllib.request.Request(web_url,
                                         data=None,
                                         headers={'User-Agent': 'Mozilla/5.0'})
            response = urllib.request.urlopen(req)
            content = response.read().decode('utf-8')
            start_title = content.find("<Title>")
            end_title = content.find("</Title>")
            filename = content[start_title:end_title]
            filename = filename.replace("<Title>", "") + str('.mp4')

            start_marker = content.find("<script type=\'text/javascript\'>eval")
            if start_marker == -1:
                raise Exception("Invalid Vidzi video")
            video_js = content[start_marker:]
            end_marker = video_js.find("</script>")
            video_js = video_js[:end_marker]
            video_js = video_js.replace("<script type=\'text/javascript\'>", "")
            beautiful_video = jsbeautifier.beautify(video_js)
            r = re.search(',\{file:"(.+?)"', beautiful_video)

            if r:
                return filename, r.group(1)
            else:
                raise Exception('File Not Found or removed')
        except Exception as e:
            print("Exception raised in Vidzi parser: " + str(e))
            raise e
