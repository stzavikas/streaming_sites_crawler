import re
import urllib.request
import urllib.parse
from time import sleep


class StreamcloudParser(object):
    def __init__(self):
        pass

    def parse_url(self, web_url):
        response = urllib.request.urlopen(web_url)
        content = response.read().decode('utf-8')

        if not re.search('id=\"btn_download\"', content):
            raise Exception('File Not Found or removed')

        form_values = {}
        for i in re.finditer(
                '<input.*?name="(.*?)".*?value="(.*?)">', content):
            form_values[i.group(1)] = i.group(2)

        form_data = urllib.parse.urlencode(form_values).encode('utf-8')
        # Wait for 10 seconds + some more as a failsafe
        sleep(11)

        content = urllib.request.urlopen(url=web_url, data=form_data).read().decode('utf-8')

        r = re.search('file: "(.+?)",', content)

        if r:
            return web_url.split("/")[-1].replace(".html", ""), r.group(1)
        else:
            raise Exception('File Not Found or removed')
