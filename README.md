# Streaming Videos Crawler #

This is a simple crawler for popular websites for streaming videos, like streamcloud.eu and similar services. It supports automatic detection and download for video links on a page, so in case you provide a url that includes several streamcloud urls, the package will automatically download all of them. Several Greek streaming video websites are supported (crawlers for xrysoi and gamato are parts of this package).


# How To Run #

You can use the crawler directly from the command line: 
```
export PYTHONPATH=<module_path>
python3 gamato_parser/main_script.py <xrysoi_url>
```

You can alternatively import the crawler as a Python package to your application, in case you want to reuse it. 

Please note that the package is Python3-compatible and does NOT work with Python2. This is really important because urllib module that is used extensively is different between the 2 Python versions.