'use strict';

var React = require("react");
var ReactDOM = require("react-dom");
var indexPage = require('./views/index.js')

var Router = require("react-router").Router;
var Route = require("react-router").Route;
var browserHistory = require("react-router").browserHistory;
var IndexRoute = require("react-router").IndexRoute;

ReactDOM.render((<div id="my_page">
        <Router history={browserHistory}>
            <Route name="homePage" path="/" component={indexPage} />
        </Router></div>), document.getElementById("main_page_div"))