'use strict';

var React = require("react");
var $ = require("jQuery");
var ReactDOM = require("react-dom");
var NetworkRequest = require("superagent");


class UrlFormComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            url_value: '',
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.submitUrl = this.submitUrl.bind(this);
    }
    handleInputChange(event) {
        this.setState({url_value: event.target.value});
    }
    submitUrl(value) {
        var url = 'http://example.com';

        NetworkRequest.get(url, function(err, res){
          if (err) throw err;
          alert(res.text);
        });
    }
    render() {
        return (<div>
                    <input type="text" value={this.state.url_value} onChange={this.handleInputChange} />
                    <button type="submit" onClick={ this.submitUrl }>Submit</button>
                </div>);
    }
};


module.exports = React.createClass({
    render() {
        return (<div id="index_div_id">
                    <div>A scraper for video files from popular platforms.</div>
                    <div>Currently supporting streamcloud.eu, hdvid.tv and vidzi.tv.</div>
                    <div id="url_form_submit"><UrlFormComponent/></div>
                </div>);
    }
});