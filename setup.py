#!/usr/bin/env python
from distutils.core import setup

setup(name='gamato_parser',
      version='1.0',
      description='Greek Streaming Sites Parser',
      author='skatokafros',
      author_email='skatokafros@hotmail.com',
      url='https://bitbucket.org/stzavikas/streaming_sites_crawler/',
      install_requires=['flask', 'gunicorn', 'requests', 'jsbeautifier'],
     )